#define N 40
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int numberOfUppercase, numberOfLowercase, numberOfNumerals;

void putchUppercase()
{
    if (numberOfUppercase>0)
    {
        putchar(rand()% 26 + 'A');
        numberOfUppercase--;
    }
    else putchLowercase();
}

void putchLowercase()
{
    if (numberOfLowercase>0)
    {
        putchar(rand()% 26 + 'a');
        numberOfLowercase--;
    }
    else putchNumeral();
}

void putchNumeral()
{
    if (numberOfNumerals>0)
    {
        putchar(rand()% 10 + '0');
        numberOfNumerals--;
    }
    else putchUppercase();
}

void generatePassword(int size)
{
    int i;
    // ��������� ������� ������������ ���������� �������� ������� ���� � ������ ���, ����� ������ ��� ���������� ���� �� 1 ���
    numberOfUppercase = rand()%(size - 2) + 1;
	numberOfLowercase = rand()%(size - numberOfUppercase - 1) + 1;
	numberOfNumerals = size - numberOfUppercase - numberOfLowercase;
	// ������� ������ ���������� ����
	// ���� �������� ����� ���� ��� ����������� ����������, ������� ������ "����������" ���� (� ������� ��������� ����� -> �������� �����-> ����� ->��������� �����...)
	for (i=0; i<size; i++)
    {
        switch(rand() % 3) //
        {
        case 0:
            putchUppercase();
            break;
        case 1:
            putchLowercase();
            break;
        case 2:
            putchNumeral();
            break;
        }
    }
}

int main()
{
	int i = 0, j = 0, passwordLength;
	srand((unsigned int)time(0));
	printf("Enter your password's length:");
	scanf("%d", &passwordLength);
	while (passwordLength<3)
    {
        printf("Error: password length is too short! ");
	    printf("Enter your password's length:");
        scanf("%d", &passwordLength);
    }
	for (j=0; j<N; j++)
    {

        generatePassword(passwordLength);
        putchar((j%2==0) ? ' ' : '\n');
    }
    return 0;
}
