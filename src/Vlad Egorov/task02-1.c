//
//  main.c
//  task02-02
//
//  Created by Vladii Egorov on 27/10/16.
//  Copyright (c) 2016 Vladii Egorov. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>


void passwordGenerator(int passwordLength)
{
  int j = 0,k = 0;
  char ch = '0';
  
  for (j = 1; j <= 2 * passwordLength; ++j)
    {
      k++;
      
      if (k == 1)
          ch = rand() % 24 + 65;
      if (k == 2)
          ch = rand() % 10 + 48;
      if (k == 3)
        {
          ch = rand() % 24 + 97;
          k = 0;
        }
    putchar(ch);
    if (j == passwordLength)
    printf("      ");
    }
}
void passwordCall(int passwordLength)
{
  int i = 0;
  srand((unsigned int)time(NULL));
  for (i = 1; i <= 20; i++)
    {
        passwordGenerator(passwordLength);
        puts("");
    }
}

int main()
{
  int length = 0;
  puts("Enter the password length:");
  scanf("%d", &length);
  printf("\v");
  passwordCall(length);
  return 0;
  }
