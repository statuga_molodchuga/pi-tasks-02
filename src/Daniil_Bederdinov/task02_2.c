#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <conio.h>
#include <string.h>

int main()
{
    char pass[] = "parol'";
    const char str[256];
    int ch;
    int i = 0;
    printf("Enter password:");
    while ((ch=_getch())!='\r')
    {
        str[i] = ch;
        printf("*");
        i++;
    }
    printf("\n");
    for (int i = 0; i < strlen(pass); i++)
    {
        if (str[i] == pass[i])
            continue;
        else 
        {
            printf("Password is not correct \n");
            return 1;
        }
    }
    printf("Password is correct! \n");
    return 0;

}