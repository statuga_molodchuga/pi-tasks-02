#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h> 
int main()
{
    float a = 0, b = 0;
    scanf("%f-%f", &a, &b);
    printf("%.0f, ", a);
    while (a != b)
    {
        a++;
        printf("%.0f, ", a);
    }

    getchar();
    getchar();
    return 0;
}
