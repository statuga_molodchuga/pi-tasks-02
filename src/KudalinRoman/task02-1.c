#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
int main()
{
	char passwrd[256];
	char symbols[] = {'A','B','C','D','E','F','G','H','I','J','K','L',
					  'M','N','O','P','Q','R','S','T','U','V','W','X',
					  'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g','h',
					  'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q','r',
					  's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0',
					  '1', '2', '3', '4', '5', '6', '7', '8', '9'};
	int i=0, length=0, j=0, nubmersCount=0, smallLatCount=0, bigLatCount=0, check=0, tabCheck=0;
	srand(time(0));
	puts("Enter a password's length (min length is 6)");
	scanf("%d", &length);
	if (length<6 && length!=0)
	{
		puts("Your password is too short!");
		return 1;
	}
	else if (length == 0)
	{
		puts("Input Error!");
		return 1;
	}
	printf("\n");
	puts("Your passwords:\n");
	length++;
	for (j=0; j<40;j++)
	{	
		check=0;
		while (check!=3)
		{	
			check=0;
			for (i = 0; i < length - 1; i++)
			{
				passwrd[i] = symbols[rand()%62];
				if (passwrd[i] >= '0' && passwrd[i] <= '9')
					nubmersCount=1;		
				else if (passwrd[i] >= 'a' && passwrd[i] <= 'z')
					smallLatCount=1;	
				else if (passwrd[i] >= 'A' && passwrd[i] <= 'Z')
					bigLatCount=1;		
			}
			check += nubmersCount+smallLatCount+bigLatCount;
			nubmersCount=0;;
			smallLatCount=0;
			bigLatCount=0;
			passwrd[length-1]=0;
			if (check != 3)			
				continue;
			else
			{ 
				if (tabCheck == 0)	 
				{	
					for (i=0; i<length; i++)
						putchar(passwrd[i]);
					printf("\t\t");
					tabCheck=1;
				}
				else if (tabCheck == 1) 
				{	
					for (i = 0; i<length; i++)
						putchar(passwrd[i]);
					printf("\n");
					tabCheck=0;
				}
			}		
		}
	}
	printf("\n");
	return 0;
}