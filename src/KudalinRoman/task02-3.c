#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int main()
{
	int leftBorder=0, rightBorder=0, i=0;
	puts("Enter the range in the following form: *num*--*num*");
	scanf("%d--%d", &leftBorder, &rightBorder);
	if (leftBorder <= 0 || rightBorder <= 0 || leftBorder > rightBorder)
	{
		puts("Input Error!");
		return 1;
	}
	printf("[");
	for (i = leftBorder; i < rightBorder; i++)
	{
		printf("%d,", i);
	}
	printf("%d]\n", i);
	return 0;
}