#include <stdio.h>
#include <conio.h>
//Написать программу, раскрывающую введенный пользователем диапазон чисел\
в последовательность чисел,\
например: "3-12" -> [3,4,5,6,7,8,9,10,11,12].\
Числа указываются только целые и положительные.

#include <string.h>
#define N 256
int main(void)
{
    const char password[N]={'p','a','s','s','w','o','r','d','\0'};
    char key[N];
    printf("Enter your password: ");
    char p;
    int i=0;
    while((p=_getch())!='\r')
    {
        if (p=='\b')
        {
            if (i>0)
            {
                i--;
                putchar('\b');//перемещаем крсор назад
                putchar(' ');//стираем *
                putchar('\b');//снова перемещаем, чтоб не было дыры
            }
        }
        else
        {
            putchar('*');
            key[i]=p;
            i++;
        }
    }
    key[i]='\0';
    putchar ('\n');
    int len = strlen(password);
    int lenKey = strlen(key);
    int right =1;//пароль введён правильно
    if (len==lenKey)
    {
        int i;
        for(i=0;i<len;i++)
        {
            if (password[i]!=key[i])
            {
                right=0;
                break;
            }
        }
    }
    else
    {
        right=0;
    }
    if (right)
    {
        printf("log in success!");
    }
    else
    {
        printf("password is incorrect");
    }

    return 0;
}

